package com.arfa.test.service

import com.arfa.test.models.User
import com.arfa.test.repository.UserRepository
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import java.util.*


fun tokenGen():String = UUID.randomUUID().toString().replace("-","")
class UserNotFound : Exception("UserNotFound")
class PasswordIsNotValid : Exception("PasswordIsNotValid")
@Service
class UserService ( private  val userRepository : UserRepository){


    fun Login (userName:String , pass : String) : Mono<Any>{

        return  userRepository.findByUserName(userName)
                .switchIfEmpty(

                    userRepository.insert(User("","",userName,pass))
                )
                .map {
                    if(it.pass != pass){
                        PasswordIsNotValid()
                    }
            it
        }

    }




}
