package com.arfa.test.repository

import com.arfa.test.models.User
import org.reactivestreams.Publisher
import org.springframework.boot.autoconfigure.security.SecurityProperties
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono


@Repository
interface UserRepository  : ReactiveMongoRepository<User,String>{

    override fun findById(id:String): Mono<User> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun findByName (name : String ): Mono<User>
    fun findByUserName ( userName : String ) : Mono<User>
    fun findByPass( pass : String ) : Mono<User>



    fun insert(user : User) : Mono<User>


}
